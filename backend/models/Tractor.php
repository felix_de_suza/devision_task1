<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tractors".
 *
 * @property int $id
 * @property string $name
 *
 * @property AreasTractors[] $areasTractors
 */
class Tractor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tractors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreasTractors()
    {
        return $this->hasMany(AreasTractors::className(), ['tractor_id' => 'id']);
    }
}
