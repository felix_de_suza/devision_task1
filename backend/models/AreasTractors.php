<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "areas_tractors".
 *
 * @property int $id
 * @property int $area_id
 * @property int $tractor_id
 * @property string $treated_area
 * @property string $treated_area_date
 *
 * @property Areas $area
 * @property Tractors $tractor
 */
class AreasTractors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'areas_tractors';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area_id', 'tractor_id'], 'required'],
            [['area_id', 'tractor_id'], 'integer'],
            [['treated_area_date'], 'safe'],
            [['treated_area'], 'string', 'max' => 255],
            [['area_id'], 'exist', 'skipOnError' => true, 'targetClass' => Area::className(), 'targetAttribute' => ['area_id' => 'id']],
            [['tractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Tractor::className(), 'targetAttribute' => ['tractor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'area_id' => 'Area ID',
            'tractor_id' => 'Tractor ID',
            'treated_area' => 'Treated Area',
            'treated_area_date' => 'Treated Area Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getArea()
    {
        return $this->hasOne(Area::className(), ['id' => 'area_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTractor()
    {
        return $this->hasOne(Tractor::className(), ['id' => 'tractor_id']);
    }
}
