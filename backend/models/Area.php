<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "areas".
 *
 * @property int $id
 * @property string $name
 * @property string $farming_culture
 * @property string $area
 *
 * @property AreasTractors[] $areasTractors
 */
class Area extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'areas';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['area'], 'integer'],
            [['name', 'farming_culture'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'farming_culture' => 'Farming Culture',
            'area' => 'Area',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAreasTractors()
    {
        return $this->hasMany(AreasTractors::className(), ['area_id' => 'id']);
    }
}
