<?php

namespace backend\controllers;

use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use Yii;
use yii\web\BadRequestHttpException;

class AreaTractorController extends ActiveController
{
    public $modelClass = 'backend\models\AreasTractors';

    public $reservedParams = [];

    public function behaviors() {

        return array_merge(parent::behaviors(), [


            $behaviors['corsFilter']  = [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:
                    'Origin'                           => ['*'],
                    'Access-Control-Request-Method'    => ['GET','POST','PUT','DELETE','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                ],
            ]

        ]);
    }

    public function actions()
    {
        $actions = parent::actions();

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $requestParams = Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = Yii::$app->getRequest()->getQueryParams();
        }

        /* @var $modelClass \yii\db\BaseActiveRecord */
        $modelClass = $this->modelClass;

        $query = $modelClass::find();

        $query->joinWith('tractor');
        $query->joinWith('area');

        if(isset($requestParams['tractor_name']) && $requestParams['tractor_name'] != ''){
            $query->where(['like','tractors.name',$requestParams['tractor_name']]);
        }


        if(isset($requestParams['area_name']) && $requestParams['area_name'] != ''){
            $query->where(['like','areas.name',$requestParams['area_name']]);
        }

        if(isset($requestParams['farming_culture']) && $requestParams['farming_culture'] != ''){
            $query->where(['like','areas.farming_culture',$requestParams['farming_culture']]);
        }

        if(isset($requestParams['treated_area_date']) && $requestParams['treated_area_date'] != '') {
            $query->where(['=', 'areas_tractors.treated_area_date', date('Y-m-d', strtotime($requestParams['treated_area_date']))]);
        }


        return Yii::createObject([
            'class' => ActiveDataProvider::className(),
            'query' => $query,
            'pagination' => [
                'params' => $requestParams,
            ],
            'sort' => [
                'params' => $requestParams,
            ],
        ]);
    }
}