<?php

namespace backend\controllers;

use yii\rest\ActiveController;

class TractorController extends ActiveController
{
    public $modelClass = 'backend\models\Tractor';

    public function behaviors() {

        return array_merge(parent::behaviors(), [


            $behaviors['corsFilter']  = [
                'class' => \yii\filters\Cors::className(),
                'cors'  => [
                    // restrict access to domains:
                    'Origin'                           => ['*'],
                    'Access-Control-Request-Method'    => ['GET','POST','PUT','DELETE','OPTIONS'],
                    'Access-Control-Request-Headers' => ['*'],
                    'Access-Control-Allow-Credentials' => true,
                    'Access-Control-Max-Age'           => 3600,                 // Cache (seconds)
                ],
            ]

        ]);
    }
}