<?php

use yii\db\Migration;
use \yii\db\Schema;

/**
 * Handles the creation of table `areas`.
 */
class m180321_115310_create_areas_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('areas', [
            'id' => $this->primaryKey(),
            'name' => Schema::TYPE_STRING,
            'farming_culture' => Schema::TYPE_STRING,
            'area' => Schema::TYPE_BIGINT
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('areas');
    }
}
