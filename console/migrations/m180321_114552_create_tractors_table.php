<?php

use yii\db\Migration;
use \yii\db\Schema;

/**
 * Handles the creation of table `tractors`.
 */
class m180321_114552_create_tractors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('tractors', [
            'id' => $this->primaryKey(),
            'name' => Schema::TYPE_STRING
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('tractors');
    }
}
