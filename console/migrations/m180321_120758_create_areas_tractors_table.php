<?php

use yii\db\Migration;

/**
 * Handles the creation of table `areas_tractors`.
 */
class m180321_120758_create_areas_tractors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('areas_tractors', [
            'id' => $this->primaryKey(),
            'area_id' => $this->integer()->notNull(),
            'tractor_id' => $this->integer()->notNull(),
            'treated_area' => $this->string(),
            'treated_area_date' => $this->dateTime()
        ]);

        $this->addForeignKey(
            'fk-post-area_id',
            'areas_tractors',
            'area_id',
            'areas',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-post-tractor_id',
            'areas_tractors',
            'tractor_id',
            'tractors',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('areas_tractors');

        $this->dropForeignKey(
            'fk-post-area_id',
            'areas'
        );

        $this->dropForeignKey(
            'fk-post-tractor_id',
            'tractors'
        );
    }
}
