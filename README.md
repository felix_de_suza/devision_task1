Installation Guide:

- run composer update command in the root directory
- create database=yii2advanced,collation=utf8_general_ci user=root, password=""
- run php init. Choose dev environment.
- run yii migrate
- setup virtual host. Example:

```
 <VirtualHost *:80>
	ServerName frontend.local
	DocumentRoot "path/to/project/frontend/web/"
	
	<Directory "path/to/project/frontend/web/">
		# use mod_rewrite for pretty URL support
		RewriteEngine on
		# If a directory or a file exists, use the request directly
		RewriteCond %{REQUEST_FILENAME} !-f
		RewriteCond %{REQUEST_FILENAME} !-d
		# Otherwise forward the request to index.php
		RewriteRule . index.php

		# use index.php as index file
		DirectoryIndex index.php

		# ...other settings...
		# Apache 2.4
		Require all granted
		
		## Apache 2.2
		Order allow,deny
		Allow from all
	</Directory>
</VirtualHost>

<VirtualHost *:80>
	ServerName backend.local
	DocumentRoot "path/to/project/backend/web/"
	
	<Directory "path/to/project/backend/web/">
		# use mod_rewrite for pretty URL support
		RewriteEngine on
		# If a directory or a file exists, use the request directly
		RewriteCond %{REQUEST_FILENAME} !-f
		RewriteCond %{REQUEST_FILENAME} !-d
		# Otherwise forward the request to index.php
		RewriteRule . index.php

		# use index.php as index file
		DirectoryIndex index.php

		# ...other settings...
		# Apache 2.4
		Require all granted
		
		## Apache 2.2
		Order allow,deny
		Allow from all
	</Directory>
</VirtualHost>
```

- add backend.local and frontend.local to hosts file
- You can visit http://frontend.local/ and login with username: admin, password: secret
