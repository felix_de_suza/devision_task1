<?php
/**
 * Created by PhpStorm.
 * User: Trayan Zlatanov
 * Date: 3/21/2018
 * Time: 7:26 PM
 */

namespace frontend\controllers;


use yii\filters\VerbFilter;
use yii\web\Controller;

class AreaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays areas list.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays new area form.
     *
     * @return string
     */
    public function actionNew()
    {
        return $this->render('new');
    }

    /**
     * Displays edit area form.
     *
     * @return string
     */
    public function actionEdit($id)
    {
        $this->view->params['id'] = $id;
        return $this->render('edit');
    }

}