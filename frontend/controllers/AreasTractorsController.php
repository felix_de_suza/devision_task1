<?php
/**
 * Created by PhpStorm.
 * User: Trayan Zlatanov
 * Date: 3/21/2018
 * Time: 7:26 PM
 */

namespace frontend\controllers;


use yii\filters\VerbFilter;
use yii\web\Controller;

class AreasTractorsController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays areas tractors list.
     *
     * @return string
     */
    public function actionIndex()
    {
        $requestParams = \Yii::$app->getRequest()->getBodyParams();
        if (empty($requestParams)) {
            $requestParams = \Yii::$app->getRequest()->getQueryParams();
        }

        $this->view->params['areaName'] = (isset($requestParams['area_name'])) ? $requestParams['area_name'] : '';
        $this->view->params['tractorName'] = (isset($requestParams['tractor_name'])) ? $requestParams['tractor_name'] : '';
        $this->view->params['farmingCulture'] = (isset($requestParams['farming_culture'])) ? $requestParams['farming_culture'] : '';
        $this->view->params['date'] =  (isset($requestParams['treated_area_date']) && $requestParams['treated_area_date'] != '')
            ? date('Y-m-d', strtotime($requestParams['treated_area_date'])) : '';

        return $this->render('index');
    }

    /**
     * Displays new area tractor form.
     *
     * @return string
     */
    public function actionNew()
    {
        return $this->render('new');
    }

    /**
     * Displays edit area tractor form.
     *
     * @return string
     */
    public function actionEdit($id)
    {
        $this->view->params['id'] = $id;
        return $this->render('edit');
    }

}