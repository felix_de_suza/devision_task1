
$(document).ready(function () {

    $('#tractor-form').submit(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var url = 'http://backend.local/tractors';
        var method = 'POST';
        var params = {
            name : $('#name').val()
        };

        $.ajax(url, {
            method : method,
            data : params
        }).done(function( data ) {
           window.location.href = '/index.php?r=tractor';
        });

        return false;
    });
});