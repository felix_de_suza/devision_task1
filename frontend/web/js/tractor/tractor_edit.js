$(document).ready(function () {
    var tractorId = $('#tractor_id').val();

    $.get('http://backend.local/tractors/' + tractorId, {}, function(data){
        $('#name').val(data.name);
    });

    $('#tractor-form').submit(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var url = 'http://backend.local/tractors/' + tractorId;
        var method = 'PUT';
        var params = {
            name : $('#name').val()
        };

        $.ajax(url, {
            method : method,
            data : params
        }).done(function( data ) {
           window.location.href = '/index.php?r=tractor';
        });

        return false;
    });
});