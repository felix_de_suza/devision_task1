$(document).ready(function () {
    $.get('http://backend.local/tractors', {}, function(data){

        $.each(data, function (i, tractor) {
            var trHtml = '<tr>\n' +
                '            <td>'+tractor.name+'</td>\n' +
                '            <td class="operations">\n' +
                '                <a href="/index.php?r=tractor/edit&id='+tractor.id+'" class="float-left"><i class="fa fa-edit" title="Edit"></i></a>' +
                '            </td>\n' +
                '        </tr>';
            $(trHtml).appendTo('#tractor-table tbody');
        });

    });
});