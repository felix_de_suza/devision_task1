
$(document).ready(function () {


    var id = $('#areas_tractors_id').val();

    $.get('http://backend.local/areas', {}, function(data){
        $.each(data, function (i, area) {
            var optionHtml = '<option value="'+area.id+'" data-max-area="'+area.area+'">'+area.name+'</option>';
            $(optionHtml).appendTo('#area_id');
        });

        $.get('http://backend.local/tractors', {}, function(data){
            $.each(data, function (i, tractor) {
                var optionHtml = '<option value="'+tractor.id+'"">'+tractor.name+'</option>';
                $(optionHtml).appendTo('#tractor_id');
            });

            $.get('http://backend.local/area-tractors/' + id, {}, function(data){
                $('#area_id').val(data.area_id).trigger('change');
                $('#tractor_id').val(data.area_id);
                $('#treated_area').val(data.treated_area);

                $('#treated_area_date').val(data.treated_area_date.slice(0,10));
            });
        });
    });

    $(document).on('change', '#area_id', function () {
        var maxArea = $(this).find('option:selected').attr('data-max-area');
        $('#treated_area').attr('max', maxArea);
    });


    $('#areas-tractors-form').submit(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var url = 'http://backend.local/area-tractors/' + id;
        var method = 'PUT';

        var params = {
            area_id : $('#area_id').val(),
            tractor_id : $('#tractor_id').val(),
            treated_area : $('#treated_area').val(),
            treated_area_date : $('#treated_area_date').val()
        };

        $.ajax(url, {
            method : method,
            data : params
        }).done(function( data ) {
           window.location.href = '/index.php?r=areas-tractors';
        });

        return false;
    });
});