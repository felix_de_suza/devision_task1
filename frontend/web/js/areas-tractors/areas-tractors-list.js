//http://backend.local/area-tractors?expand=area.name,tractor.name&treated_area_date=21.03.2018

$(document).ready(function () {
    var params = {
        area_name: $('#area_name').val(),
        tractor_name: $('#tractor_name').val(),
        farming_culture: $('#farming_culture').val(),
        treated_area_date: $('#treated_area_date').val(),
        expand : 'area.name,tractor.name'

    };


    $.get('http://backend.local/area-tractors', params, function(data){
        var totalArea = 0;

        $.each(data, function (i, row) {
            var trHtml = '<tr>\n' +
                '            <td>'+row.area.name+'</td>\n' +
                '            <td>'+row.area.farming_culture+'</td>\n' +
                '            <td>'+row.tractor.name+'</td>\n' +
                '            <td>'+row.treated_area_date+'</td>\n' +
                '            <td>'+row.treated_area+'</td>\n' +
                '            <td class="operations">\n' +
                '                <a href="/index.php?r=areas-tractors/edit&id='+row.id+'" class="float-left"><i class="fa fa-edit" title="Edit"></i></a>' +
                '            </td>\n' +
                '        </tr>';
            $(trHtml).appendTo('#areas-tractors-table tbody');
            totalArea += parseInt(row.treated_area);
        });

        $('#treated-area-total').html(totalArea);

    });
});