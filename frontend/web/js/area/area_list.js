$(document).ready(function () {
    $.get('http://backend.local/areas', {}, function(data){

        $.each(data, function (i, area) {
            var trHtml = '<tr>\n' +
                '            <td>'+area.name+'</td>\n' +
                '            <td>'+area.farming_culture+'</td>\n' +
                '            <td>'+area.area+'</td>\n' +
                '            <td class="operations">\n' +
                '                <a href="/index.php?r=area/edit&id='+area.id+'" class="float-left"><i class="fa fa-edit" title="Edit"></i></a>' +
                '            </td>\n' +
                '        </tr>';
            $(trHtml).appendTo('#area-table tbody');
        });

    });
});