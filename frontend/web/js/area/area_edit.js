$(document).ready(function () {
    var areaId = $('#area_id').val();

    $.get('http://backend.local/areas/' + areaId, {}, function(data){
        $('#name').val(data.name);
        $('#farming_culture').val(data.farming_culture);
        $('#area').val(data.area);
    });

    $('#area-form').submit(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var url = 'http://backend.local/areas/' + areaId;
        var method = 'PUT';
        var params = {
            name : $('#name').val(),
            farming_culture : $('#farming_culture').val(),
            area : $('#area').val()
        };

        $.ajax(url, {
            method : method,
            data : params
        }).done(function( data ) {
           window.location.href = '/index.php?r=area';
        });

        return false;
    });
});