
$(document).ready(function () {

    $('#area-form').submit(function (e) {
        e.preventDefault();
        e.stopPropagation();

        var url = 'http://backend.local/areas';
        var method = 'POST';
        var params = {
            name : $('#name').val(),
            farming_culture : $('#farming_culture').val(),
            area : $('#area').val()
        };

        $.ajax(url, {
            method : method,
            data : params
        }).done(function( data ) {
           window.location.href = '/index.php?r=area';
        });

        return false;
    });
});