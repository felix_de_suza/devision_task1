<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'bootstrap/css/bootstrap.min.css',
        'font-awesome/css/font-awesome.min.css',
        'datatables/dataTables.bootstrap4.css',
        'css/sb-admin.css',
        'css/app.css',
    ];
    public $js = [
        'jquery/jquery.min.js',
        'bootstrap/js/bootstrap.bundle.min.js',
        'jquery-easing/jquery.easing.min.js',
        'js/sb-admin.min.js',
        'js/app.js',
    ];
    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
