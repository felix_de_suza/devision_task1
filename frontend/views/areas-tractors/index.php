<?php

/* @var $this yii\web\View */

$this->title = 'Areas to tractors list';

$this->registerJsFile('js/areas-tractors/areas-tractors-list.js',['depends' => [
    \yii\web\JqueryAsset::className()
]]);

?>
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <?= \yii\helpers\Html::a('Areas to tractors', ['areas-tractors/index']) ?>
    </li>
    <li class="breadcrumb-item active">List</li>
</ol>
<?= \yii\helpers\Html::a('<i class="fa fa-plus"></i> Create new area to tractor', ['areas-tractors/new'], ['class' => 'btn btn-primary btn-success float-right']) ?>
<div class="row">
    <div class="card mx-auto col-md-12">
        <div class="card-header">Edit tractor</div>
        <div class="card-body">
            <form action="/index.php">
                <input type="hidden" name="r" value="areas-tractors">
                <div class="row">
                    <div class="form-group col-md-3">
                        <label for="area_name">Area</label>
                        <input type="text" name="area_name" class="form-control" id="area_name" value="<?=$this->params['areaName']?>" placeholder="Filter by Area Name">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="tractor_name">Tractor</label>
                        <input type="text" name="tractor_name" class="form-control" id="tractor_name" value="<?=$this->params['tractorName']?>" placeholder="Filter by Tractor Name">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="farming_culture">Farming culture</label>
                        <input type="text" name="farming_culture" class="form-control" id="farming_culture" value="<?=$this->params['farmingCulture']?>" placeholder="Filter by Farming culture">
                    </div>
                    <div class="form-group col-md-3">
                        <label for="farming_culture">Date</label>
                        <input type="date" name="treated_area_date" class="form-control" id="treated_area_date" value="<?=$this->params['date']?>" placeholder="Filter by Date">
                    </div>
                    <button type="submit" id="save" class="btn btn-primary btn-block col-md-2">Search</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="table-responsive">
    <table class="table table-bordered" id="areas-tractors-table" width="100%" cellspacing="0">
        <thead>
        <tr>
            <th>Area</th>
            <th>Farming culture</th>
            <th>Tractor</th>
            <th>Date</th>
            <th>Treated area</th>
            <th style="width: 120px">Actions</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
        <tfoot>
            <tr>
                <th colspan="4">Total</th>
                <th id="treated-area-total"></th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>
