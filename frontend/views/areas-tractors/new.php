<?php

/* @var $this yii\web\View */

$this->title = 'New Area to tractor';

$this->registerJsFile('js/areas-tractors/areas-tractors_new.js',['depends' => [
    \yii\web\JqueryAsset::className()
]]);

?>
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <?= \yii\helpers\Html::a('Areas to tractors', ['areas-tractors/index']) ?>
    </li>
    <li class="breadcrumb-item active">New Area to tractor</li>
</ol>
<div class="row">
    <div class="card mx-auto col-md-10">
        <div class="card-header">New Area to tractor</div>
        <div class="card-body">
            <form id="areas-tractors-form">
                <div class="form-group">
                    <label for="area_id">Choose area</label>
                    <select name="area_id" id="area_id" class="form-control" required>
                        <option value="" data-max-area="0"></option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="tractor_id">Choose tractor</label>
                    <select name="tractor_id" id="tractor_id" class="form-control" required>
                        <option value=""></option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="name">Enter treatment area</label>
                    <input type="number" name="treated_area" class="form-control" id="treated_area" value="" placeholder="Enter treatment area" max="0" required>
                </div>
                <div class="form-group">
                    <label for="name">Enter date</label>
                    <input type="date" name="treated_area_date" class="form-control" id="treated_area_date" value="" placeholder="Enter date" required>
                </div>
                <div class="row">
                    <button type="submit" id="save" class="btn btn-primary btn-block col-md-3">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>