<?php

/* @var $this yii\web\View */

$this->title = 'Area edit';

$this->registerJsFile('js/area/area_edit.js',['depends' => [
    \yii\web\JqueryAsset::className()
]]);

?>
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <?= \yii\helpers\Html::a('Areas', ['area/index']) ?>
    </li>
    <li class="breadcrumb-item active">Edit area</li>
</ol>
<div class="row">
    <div class="card mx-auto col-md-10">
        <div class="card-header">Edit area</div>
        <div class="card-body">
            <form id="area-form">
                <div class="form-group">
                    <label for="name">Enter Name</label>
                    <input type="text" name="name" class="form-control" id="name" value="" placeholder="Enter Name" required>
                </div>
                <div class="form-group">
                    <label for="name">Enter farming culture</label>
                    <input type="text" name="farming_culture" class="form-control" id="farming_culture" value="" placeholder="Enter farming culture" required>
                </div>
                <div class="form-group">
                    <label for="name">Enter area</label>
                    <input type="number" name="area" class="form-control" id="area" value="" placeholder="Enter area" required>
                </div>
                <div class="row">
                    <button type="submit" id="save" class="btn btn-primary btn-block col-md-3">Save</button>
                </div>
                <input type="hidden" id="area_id" value="<?php echo $this->params['id']?>">
            </form>
        </div>
    </div>
</div>