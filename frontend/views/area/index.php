<?php

/* @var $this yii\web\View */

$this->title = 'Areas list';

$this->registerJsFile('js/area/area_list.js',['depends' => [
    \yii\web\JqueryAsset::className()
]]);

?>
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <?= \yii\helpers\Html::a('Areas', ['area/index']) ?>
    </li>
    <li class="breadcrumb-item active">List</li>
</ol>
<?= \yii\helpers\Html::a('<i class="fa fa-plus"></i> Create new area', ['area/new'], ['class' => 'btn btn-primary btn-success float-right']) ?>
<div class="table-responsive">
    <table class="table table-bordered" id="area-table" width="100%" cellspacing="0">
        <thead>
        <tr>
            <th>Name</th>
            <th>Farming culture</th>
            <th>Area</th>
            <th style="width: 120px">Actions</th>
        </tr>
        </thead>
        <tbody>

        </tbody>
    </table>
</div>
