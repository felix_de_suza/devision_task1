<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>

    <?php
//    if (Yii::$app->user->isGuest) {
//        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
//    } else {
//
//    }

    ?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
<?php $this->beginBody() ?>
<!-- Navigation-->
<?php if (!Yii::$app->user->isGuest) { ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="/">Devision</a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Home">
                <a class="nav-link" href="/">
                    <i class="fa fa-fw fa-dashboard"></i>
                    <span class="nav-link-text">Home</span>
                </a>
            </li>

            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Areas">
                <?= \yii\helpers\Html::a('<i class="fa fa-fw fa-file"></i><span class="nav-link-text">Areas</span>'
                    , ['area/index'], ['class' => 'nav-link']) ?>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tractors">
                <?= \yii\helpers\Html::a('<i class="fa fa-fw fa-file"></i><span class="nav-link-text">Tractors</span>'
                    , ['tractor/index'], ['class' => 'nav-link']) ?>
            </li>
            <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Areas to tractors">
                <?= \yii\helpers\Html::a('<i class="fa fa-fw fa-file"></i><span class="nav-link-text">Areas to tractors</span>'
                    , ['areas-tractors/index'], ['class' => 'nav-link']) ?>
            </li>

        </ul>

        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a href="#" class="nav-link dropdown-toggle mr-lg-2" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true" v-pre>
                    <?php echo Yii::$app->user->identity->username?> <span class="caret"></span>
                </a>
                <div class="dropdown-menu" aria-labelledby="messagesDropdown">
                    <a href="#void" class="dropdown-item"
                       onclick="event.preventDefault();
                       document.getElementById('logout-form').submit();">
                        <i class="fa fa-fw fa-sign-out"></i> Logout
                    </a>
                    <?php
                    echo Html::beginForm(['/site/logout'], 'post', ['id' => 'logout-form']);
                    echo Html::endForm();
                    ?>
                </div>

            </li>
        </ul>
    </div>
</nav>
<?php } ?>
<div class="content-wrapper">
    <div class="container-fluid">
<!--        @if(\Illuminate\Support\Facades\Session::has('success_message'))-->
<!--        <div class="alert alert-success">-->
<!--            {{\Illuminate\Support\Facades\Session::get('success_message')}}-->
<!--        </div>-->
<!--        @endif-->
<!--        @if(\Illuminate\Support\Facades\Session::has('error_message'))-->
<!--        <div class="alert alert-danger">-->
<!--            {{\Illuminate\Support\Facades\Session::get('error_message')}}-->
<!--        </div>-->
<!--        @endif-->
        <?= $content ?>

    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
        <div class="container">
            <div class="text-center">
                <small>Copyright © Your Website 2018</small>
            </div>
        </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fa fa-angle-up"></i>
    </a>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


