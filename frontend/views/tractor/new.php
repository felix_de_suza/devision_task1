<?php

/* @var $this yii\web\View */

$this->title = 'New tractor';

$this->registerJsFile('js/tractor/tractor_new.js',['depends' => [
    \yii\web\JqueryAsset::className()
]]);

?>
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <?= \yii\helpers\Html::a('Tractors', ['tractor/index']) ?>
    </li>
    <li class="breadcrumb-item active">Create new tractor</li>
</ol>
<div class="row">
    <div class="card mx-auto col-md-10">
        <div class="card-header">Create new tractor</div>
        <div class="card-body">
            <form id="tractor-form">
                <div class="form-group">
                    <label for="name">Enter Name</label>
                    <input type="text" name="name" class="form-control" id="name" value="" placeholder="Enter Name" required>
                </div>
                <div class="row">
                    <button type="submit" id="save" class="btn btn-primary btn-block col-md-3">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>