<?php

/* @var $this yii\web\View */

$this->title = 'Tractor edit';

$this->registerJsFile('js/tractor/tractor_edit.js',['depends' => [
    \yii\web\JqueryAsset::className()
]]);

?>
<!-- Breadcrumbs-->
<ol class="breadcrumb">
    <li class="breadcrumb-item">
        <?= \yii\helpers\Html::a('Tractors', ['tractor/index']) ?>
    </li>
    <li class="breadcrumb-item active">Edit tractor</li>
</ol>
<div class="row">
    <div class="card mx-auto col-md-10">
        <div class="card-header">Edit tractor</div>
        <div class="card-body">
            <form id="tractor-form">
                <div class="form-group">
                    <label for="name">Enter Name</label>
                    <input type="text" name="name" class="form-control" id="name" value="" placeholder="Enter Name" required>
                </div>
                <div class="row">
                    <button type="submit" id="save" class="btn btn-primary btn-block col-md-3">Save</button>
                </div>
                <input type="hidden" id="tractor_id" value="<?php echo $this->params['id']?>">
            </form>
        </div>
    </div>
</div>